﻿# Check if running as Administrator
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    # Relaunch as administrator
    Start-Process PowerShell -ArgumentList "-File `"$($MyInvocation.MyCommand.Definition)`"" -Verb RunAs
    exit
}

# Specify the directory and files
$directory = "C:\ProgramData\Streambox\SpectraUI\log"
$files = @("pluglog.txt", "asiolog.txt", "encoder.log")

# Remove specified files if they exist
foreach ($file in $files) {
    $filePath = Join-Path -Path $directory -ChildPath $file
    # Check if file exists and remove it
    if (Test-Path $filePath) {
        Remove-Item -Path $filePath
    }
}
