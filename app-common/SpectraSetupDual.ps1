# Script to Create Spectra DUAL-CHANNEL encoder
# Script creates Symbolic links to Specta.2 and SpectraUI.2 to allow main Spectra normal update
# Script installs Spectra5.2 and Spectra3.2 services 
# v1.02
#Create Copy of Spectra and Spectra UI

$FSTREAMBOX='C:\Program Files\Streambox'
$FDATA='C:\ProgramData\Streambox'
$folderUI='SpectraUI.2' # Specta UI subbfolder
$folderSP='Spectra.2'
$FSPECTRA="$FSTREAMBOX\Spectra";
$ENV:PATH+=";$FSPECTRA"

param([switch]$Elevated)

function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
    exit
}

'running with full privileges'

#Copy "C:\ProgramData\Streambox\SpectraUI" to "C:\ProgramData\Streambox\$folderUI"
#Copy "C:\Program Files\Streambox\Spectra" "C:\Program Files\Streambox\$folderSP"

#Copy-Item "$FDATA\SpectraUI" "$FDATA\$folderUI" -recurse -Force
#Copy-Item "$FSTREAMBOX\Spectra" "$FSTREAMBOX\$folderSP" -recurse -Force

# Create Spectra.2 folder and dist subfolder
New-Item -Path "$FSTREAMBOX\" -Name "$folderSP" -ItemType Directory
New-Item -Path "$FSTREAMBOX\$folderSP" -Name "dist" -ItemType Directory

#create New SpectraUI.2
New-Item -Path  "$FDATA\$folderUI" -ItemType Directory -Force
New-Item -Path  "$FDATA\$folderUI\log" -ItemType Directory -Force

function make-link ($target, $link) { 
    New-Item -Type SymbolicLink  -Target  $target  -Path $link
}


#New-Item -Type SymbolicLink  -Target  "$FSTREAMBOX\Spectra\dist\set_defaults.exe"  -Path "$FSTREAMBOX\$folderSP\dist\set_defaults.exe"
#New-Item -Type SymbolicLink  -Target  "$FSTREAMBOX\Spectra\dist\chromactivate.exe"  -Path "$FSTREAMBOX\$folderSP\dist\chromactivate.exe"
#New-Item -Type SymbolicLink  -Target  "$FSTREAMBOX\Spectra\dist\curl"  -Path "$FSTREAMBOX\$folderSP\dist\curl"
#New-Item -Type SymbolicLink  -Target  "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe"  -Path "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe"

#symbolic link to common data
make-link -Target  "$FSPECTRA\dist\set_defaults.exe"        -Link "$FSTREAMBOX\$folderSP\dist\set_defaults.exe"
make-link -Target  "$FSPECTRA\dist\chromactivate.exe"       -Link "$FSTREAMBOX\$folderSP\dist\chromactivate.exe"
make-link -Target  "$FSPECTRA\dist\curl"                    -Link "$FSTREAMBOX\$folderSP\dist\curl"
#make-link -Target  "$FSPECTRA\dist\SpectraControlPanel.exe" -Link "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe"

#NEED to CREAT HARD-LINK, because SPC.exe is using target EXE location rather then startup folder
New-Item -Type HardLink  -Target  "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe"  -Path "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe"

#Remove
#Copy-Item   "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe" "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe" -Force

get-acl "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe" | set-acl "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe" 


Copy-Item "$FSPECTRA\dist\encassist.ini"  "$FSTREAMBOX\$folderSP\dist"   -Force 


#symbolic links to common data
make-link -Target  "$FDATA\SpectraUI\hash"     -Link "$FDATA\$folderUI\hash"
make-link -Target  "$FDATA\SpectraUI\liveServers.xml"     -Link "$FDATA\$folderUI\liveServers.xml"
Copy-Item "$FDATA\SpectraUI\settings.xml"   "$FDATA\$folderUI\settings.xml"   -Force 

#settings.xml
#New-Item -Type SymbolicLink

# encassist.ini chromactivate.exe set_defaults.exe SpectraControlPanel.exe curl
#Stop any spectra sevices

#modify/add ( Spectra1 to daisy chain video to spectra channel 1)
$settings = "$FDATA\SpectraUI\settings.xml"
$found = Select-String -Path $settings -Pattern "ChainCapture"
#ChainCapture="1" ChainChannel="1"
if ($found -eq $null){
    # no modification was made yet!
    (Get-Content $settings) -replace '<SB_HD_ENCODER ', '<SB_HD_ENCODER ChainCapture="1" ChainChannel="1" ' | Set-Content $settings
}
#Get-Content $settings

#modify/add ( Spectra2 to receive daisy chained video, change Encoder PHP and use "anyport" on second instance)
#SpectraChannel="1" PHPENCODER="1803" anyport="1"
$settings = "$FDATA\$folderUI\settings.xml"
$found = Select-String -Path $settings -Pattern "SpectraChannel"
if ($found -eq $null){
    # wite Daisy chaing spectra to CH1
    (Get-Content $settings) -replace '<SB_HD_ENCODER ', '<SB_HD_ENCODER SpectraChannel="1" PHPENCODER="1803" anyport="1" ' | Set-Content $settings
}


#Get-Content $settings

#Note - Spectra2 should be in "Spectra" source mode.
#Spectra1 - can be any source 

#modify Spectra control panel option
#C:\Program Files\Streambox\$folderSP\dist\encassist.ini
#enc_port=1803
$enca = "$FSTREAMBOX\$folderSP\dist\encassist.ini"

(Get-Content $enca) -replace '; enc_port=1802', 'enc_port=1803' | Set-Content $enca
(Get-Content $enca) -replace '; enc_ip=127.0.0.1', 'enc_ip=127.0.0.1' | Set-Content $enca

$found = Select-String -Path $enca -Pattern "spectra5.2"
if ($found -eq $null){
    (Get-Content $enca) -replace 'Service spectra5', 'Service spectra5.2' | Set-Content $enca
    (Get-Content $enca) -replace 'Service spectra3', 'Service spectra3.2' | Set-Content $enca
}

#let SCP know to use Spectra5.2 Spectra3.2 services and show Channel#2 in Title
#ws-name-spectra3 = Spectra3.2
#ws-name-spectra5 = Spectra5.2
#app-title-suffix = INSTANCE.2
$found = Select-String -Path $enca -Pattern "CHANNEL#2"
if ($found -eq $null){
    (Get-Content $enca) -replace "\[encassist\]", "[encassist]`n`napp-title-suffix=CHANNEL#2`nws-name-spectra3 = spectra3.2`nws-name-spectra5 = spectra5.2" | Set-Content $enca
}

#Get-Content $enca

# SERVICES


function InstallService {
    $s = Get-Service | Where-Object { $_.Name -eq "$service" }
    #nssm remove $service confirm
    if (! $s) {
       &"C:\Program Files\Streambox\Spectra\nssm.exe" install $service "$FSPECTRA\$exe.exe"
    }
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service Start SERVICE_DEMAND_START
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppRotateFiles 4
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppRotateOnline 0
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppRotateSeconds (New-Timespan -Days 7).TotalSeconds
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppRotateBytes 1048576
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppStdoutCreationDisposition 4
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppStderrCreationDisposition 4
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service DisplayName "Streambox $service"
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppParameters "-xml $FDATA\$folderUI\settings.xml"
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppStdout "$FDATA\$folderUI\log\encoder.log"
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppStderr "$FDATA\$folderUI\log\encoder.log"
    &"C:\Program Files\Streambox\Spectra\nssm.exe" set $service AppEvents Exit/Post 'C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Minimized -File ""C:\Program Files\Streambox\folderSP\cleanlogs.ps1""'
}

# stop and remove for re-install
Stop-Service Spectra5.2
Stop-Service Spectra3.2
nssm remove Spectra5.2 confirm
nssm remove Spectra3.2 confirm

$service = 'Spectra5.2'
$exe = 'Encoder5'
InstallService

$service = 'Spectra3.2'
$exe = 'Encoder3'
InstallService


#Create Shortcut to CH1
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\Spectra-CH1.lnk")
$Shortcut.TargetPath = "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe"
$Shortcut.WorkingDirectory =  "$FSTREAMBOX\Spectra\dist\"
$Shortcut.Save()

#Create Shortcut to CH2
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\Spectra-CH2.lnk")
$Shortcut.TargetPath = "$FSTREAMBOX\$folderSP\dist\SpectraControlPanel.exe"
$Shortcut.WorkingDirectory =  "$FSTREAMBOX\$folderSP\dist\"
$Shortcut.Save()

#TODO : Add Spectra CH Shortcut in "Start Menu"

# Enable non-admin to start/stop service
$service_names = @(
    "Spectra5.2"
    "Spectra3.2"
)

foreach ($name in $service_names){
    $service = Get-Service | Where-Object {$_.Name -eq $name }
    If(!$service){
        Write-Warning "Hmmm, I can't find service ${name}.  Is this expected?"
        continue
    }
    &C:\Windows\System32\sc.exe sdset $name "D:AR(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWRPWPDTLOCRRC;;;BU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
}

