function ql { $args }

class SpectraManager {
    [string]$InstallDir
    [string]$PanelPath
    [string]$SetDefaultsPath
    [string]$NssmPath
    [string]$PanelLogDir
    [hashtable]$Services
    [string]$HashesDir

    SpectraManager($path) {
        $this.InstallDir = $path
        $this.PanelPath = Join-Path $path dist | Join-Path -ChildPath SpectraControlPanel.exe
        $this.SetDefaultsPath = Join-Path $path dist | Join-Path -ChildPath set_defaults.exe
        $this.NssmPath = Join-Path $path nssm.exe
        $this.PanelLogDir = Join-Path $path dist
        $this.HashesDir = 'C:\ProgramData\Streambox\Spectra\hash'

        $this.Services = @{ }

        $this.Services.Spectra3 = @{ }
        $this.Services.Spectra3.binpath = Join-Path $path Encoder3.exe
        $this.Services.Spectra3.process = 'Encoder3'

        $this.Services.Spectra5 = @{ }
        $this.Services.Spectra5.binpath = Join-Path $path Encoder5.exe
        $this.Services.Spectra5.process = 'Encoder5'
    }

    InstallService($service) {
        $s = Get-Service | Where-Object { $_.Name -eq "$service" }
        if (! $s) {
            &$this.NssmPath install $service $this.Services.$service.binpath
        }
        &$this.NssmPath set $service Start SERVICE_DEMAND_START
        &$this.NssmPath set $service AppRotateFiles 4
        &$this.NssmPath set $service AppRotateOnline 0
        &$this.NssmPath set $service AppRotateSeconds (New-Timespan -Days 7).TotalSeconds
        &$this.NssmPath set $service AppRotateBytes 1048576
        &$this.NssmPath set $service AppStdoutCreationDisposition 4
        &$this.NssmPath set $service AppStderrCreationDisposition 4
        &$this.NssmPath set $service DisplayName "Streambox $service"
        &$this.NssmPath set $service AppParameters '-xml "C:\ProgramData\Streambox\SpectraUI\settings.xml"'
        &$this.NssmPath set $service AppParameters '-xml "C:\ProgramData\Streambox\SpectraUI\settings.xml"'
        &$this.NssmPath set $service AppStdout "C:\ProgramData\Streambox\SpectraUI\log\encoder.log"
        &$this.NssmPath set $service AppStderr "C:\ProgramData\Streambox\SpectraUI\log\encoder.log"
        &$this.NssmPath set $service AppEvents Exit/Post 'C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Minimized -File ""C:\Program Files\Streambox\Spectra\cleanlogs.ps1""'
        $this.SetServicePermissions($service)
    }
    
    SetServicePermissions($service) {
        # enable all users to start/stop service
        Set-Location C:\Windows\System32
        &.\sc.exe sdset $service "D:AR(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;IU)(A;;CCLCSWRPWPDTLOCRRC;;;BU)S:(AU;FA;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;WD)"
    }

    UninstallService($service) {
        $s = Get-Service | Where-Object { $_.Name -eq "$service" }
        if ($s) {
            $this.StopService($service)
            $output = &$this.NssmPath remove $service confirm
            if (! $?) {
                throw "UninstallService: $output"
            }
        }
    }

    RestartService($service) {
        $this.StopService($service)
        $this.StartService($service)
    }

    StopService($service) {
        $s = Get-Service | Where-Object { $_.Name -eq "$service" }
        if ($s) {
            if ($s.Status -eq "Running") {
                $output = &$this.NssmPath stop $service
                if (! $?) {
                    throw "StopService: $output"
                }
            }
        }
        $this.StopProcess($this.Services.$service.process)
    }

    StartService($service) {
        $s = Get-Service | Where-Object { $_.Name -eq "$service" }
        if ($s) {
            if ($s.Status -ne "Running") {
                $output = &$this.NssmPath start $service
                if (! $?){
                    throw "StartService: $output"
                }
            }
        } else {
            $this.InstallService($service)
            $output = &$this.NssmPath start $service
            if (! $?) {
                throw "StartService: $output"
            }
        }
    }

    StopSpectra() {
        $this.StopAll()
    }

    StartACTL5() {
        $this.StopAll()
        $this.RestartService("Spectra5")
        Start-Process $this.PanelPath
    }

    StartACTL3() {
        $this.StopAll()
        $this.RestartService("Spectra3")
        Start-Process $this.PanelPath
    }

    StartPanel() {
        $this.StopProcess("SpectraControlPanel")
        Start-Process $this.PanelPath
    }

    RemoveExtras() {
        $list = @(
            Join-Path $this.PanelLogDir SpectraControl*.log*
            Join-Path $this.InstallDir UHD.txt
        )
        foreach ($extra in $list) {
            Remove-Item -Force -ErrorAction Ignore $extra
        }
    }

    StopAll() {
        $plist = ql sbxcmd SpectraControlPanel $this.Services.Spectra3.process $this.Services.Spectra5.process

        $this.StopService("Spectra3")
        $this.StopService("Spectra5")
        $this.StopProcessArray($plist)
    }

    StopProcessArray($processes) {
        foreach ($process in $processes) {
            $this.StopProcess($process)
        }
    }

    StopProcess($process) {
        function _Stop($process) {
            Get-Process | Where-Object { $_.Name -eq "$process" } | Stop-Process -Force

            Start-Sleep -Seconds 0.25

            # try harder
            $p = get-wmiobject -Class Win32_Process -Filter "name like '$process%'"
            if ($p) {
               try { $p.terminate() } catch { Write-Error "WMI terminate is obsolete" }
            }
        }

        _Stop($process)
        $tries = 1
        While(($tries -lt 5) -and (Get-Process | Where-Object { $_.Name -eq "$process" })){
            Start-Sleep -Seconds 0.25
            $tries += 1
            _Stop($process)
        }
    }

    PanelSetDefaults() {
        $here = Split-Path -Parent $this.SetDefaultsPath
        Set-Location $here
        &.\set_defaults.exe
    }

    FirewallRulesAdd() {
        $this.FirewallRulesRemove() # prevent duplicates
        &C:\Windows\System32\netsh.exe advfirewall firewall add rule name="Streambox Spectra Encoder3" dir=in action=allow program="$($this.Services.Spectra3.binpath)" enable=yes profile=Any
        &C:\Windows\System32\netsh.exe advfirewall firewall add rule name="Streambox Spectra Encoder5" dir=in action=allow program="$($this.Services.Spectra5.binpath)" enable=yes profile=Any
    }

    FirewallRulesRemove() {
        &C:\Windows\System32\netsh.exe advfirewall firewall delete rule name="Streambox Spectra Encoder3"
        &C:\Windows\System32\netsh.exe advfirewall firewall delete rule name="Streambox Spectra Encoder5"
    }
}

class ShortcutManager {
    ApplyRunAsAdmin($lnk) {
        $bytes = [System.IO.File]::ReadAllBytes($lnk)
        $bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
        [System.IO.File]::WriteAllBytes($lnk, $bytes)
    }
}
