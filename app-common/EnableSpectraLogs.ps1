﻿# Check if running as Administrator
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    # Relaunch as administrator
    Start-Process PowerShell -ArgumentList "-File `"$($MyInvocation.MyCommand.Definition)`"" -Verb RunAs
    exit
}

# Specify the directory and files
$directory = "C:\ProgramData\Streambox\SpectraUI\log"
$files = @("pluglog.txt", "asiolog.txt", "encoder.log")

# Create or truncate files
foreach ($file in $files) {
    $filePath = Join-Path -Path $directory -ChildPath $file
    # Check if file exists, truncate it; if not, create it
    if (Test-Path $filePath) {
        Clear-Content -Path $filePath
    } else {
        New-Item -Path $filePath -ItemType File
    }

    # Set file access rights to 'Everyone'
    $acl = Get-Acl $filePath
    $permission = "Everyone", "FullControl", "Allow"
    $accessRule = New-Object System.Security.AccessControl.FileSystemAccessRule $permission
    $acl.SetAccessRule($accessRule)
    $acl | Set-Acl $filePath
}

