using module .\control.psm1

Param(
  [Parameter(Mandatory=$true)]
  [string]$Command
)

$m = New-Object SpectraManager($(pwd))

switch ($Command) {
    "PanelSetDefaults"          { $m.PanelSetDefaults(); break; }
    "InstallServiceSpectra3"    { $m.InstallService("Spectra3"); break; }
    "InstallServiceSpectra5"    { $m.InstallService("Spectra5"); break; }
    "RestartServiceSpectra3"    { $m.RestartService("Spectra3"); break; }
    "RestartServiceSpectra5"    { $m.RestartService("Spectra5"); break; }
    "StopServiceSpectra3"       { $m.StopService("Spectra3"); break; }
    "StopServiceSpectra5"       { $m.StopService("Spectra5"); break; }
    "FirewallRulesAdd"          { $m.FirewallRulesAdd(); break; }
    "FirewallRulesRemove"       { $m.FirewallRulesRemove(); break; }
    "Start-ACTL3"               { $m.StartACTL3(); break; }
    "Start-ACTL5"               { $m.StartACTL5(); break; }
    "Start-Panel"               { $m.StartPanel(); break; }
    "StopSpectra"               { $m.StopSpectra(); break; }
    "RemoveExtras"              { $m.RemoveExtras(); break; }
    "StopAll"                   { $m.StopAll(); break; }
    "UninstallServiceSpectra5"  { $m.UninstallService("Spectra5"); break; }
    "RemoveLegacyService"       { $m.UninstallService("StreamboxSpectra"); break; }
    "UninstallServiceSpectra3"  { $m.UninstallService("Spectra3"); break; }
    default { 
        "Something else happened"
        break 
    }
}
