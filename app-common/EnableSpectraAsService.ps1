﻿# EnableSpectraAsService.ps1 script changes Spectra to Run as a Service (default)
# See DisableSpectraAsService.ps1 to run Spectra in User Space for Audio Monitoring

# Check if running as Administrator
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    # Relaunch as administrator
    Start-Process PowerShell -ArgumentList "-File `"$($MyInvocation.MyCommand.Definition)`"" -Verb RunAs
    exit
}

# Attempt to stop services silently
Stop-Service spectra3 -ErrorAction SilentlyContinue
Stop-Service spectra5 -ErrorAction SilentlyContinue

# Attempt to stop processes silently if they exist
$processes = @("Encoder5", "Encoder3","SpectraControlPanel")
foreach ($process in $processes) {
    $proc = Get-Process -Name $process -ErrorAction SilentlyContinue
    if ($proc) {
        $proc | Stop-Process -Force -ErrorAction SilentlyContinue
    }
}

# Backup the encassist.ini file
$sourceFile = "C:\Program Files\Streambox\Spectra\dist\encassist.ini"
$backupFile = $sourceFile + ".bak"
Copy-Item $sourceFile -Destination $backupFile -Force

# Read content excluding specific lines
$content = Get-Content $sourceFile | Where-Object { $_ -notmatch "cmd-ps-ws-stop-spectra|cmd-ps-ws-start-spectra|app-icon-is-enc-status|polling-apps-php-port" }

# Add new lines
$newLines = @"
cmd-ps-ws-stop-spectra5=C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Hidden -Command Stop-Service spectra5
cmd-ps-ws-stop-spectra3=C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Hidden -Command Stop-Service spectra3
cmd-ps-ws-start-spectra5=C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Hidden -Command Start-Service spectra5
cmd-ps-ws-start-spectra3=C:\Windows\System32\windowspowershell\v1.0\powershell.exe -WindowStyle Hidden -Command Start-Service spectra3
"@

# Write back to the file
$content | Set-Content $sourceFile
Add-Content -Path $sourceFile -Value $newLines
