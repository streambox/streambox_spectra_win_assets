# Script to Unistall Spectra DUAL-CHANNEL encoder
# v1.02

$FSTREAMBOX='C:\Program Files\Streambox'
$FDATA='C:\ProgramData\Streambox'
$folderUI='SpectraUI.2' # Specta UI subbfolder
$folderSP='Spectra.2'
$FSPECTRA="$FSTREAMBOX\Spectra";


param([switch]$Elevated)

function Test-Admin {
    $currentUser = New-Object Security.Principal.WindowsPrincipal $([Security.Principal.WindowsIdentity]::GetCurrent())
    $currentUser.IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

if ((Test-Admin) -eq $false)  {
    if ($elevated) {
        # tried to elevate, did not work, aborting
    } else {
        Start-Process powershell.exe -Verb RunAs -ArgumentList ('-noprofile -noexit -file "{0}" -elevated' -f ($myinvocation.MyCommand.Definition))
    }
    exit
}

'running with full privileges'

# stop and remove for re-install
Stop-Service Spectra5.2
Stop-Service Spectra3.2
nssm remove Spectra5.2 confirm
nssm remove Spectra3.2 confirm

Remove-Item  -Path "$FSTREAMBOX\$folderSP" -Force -recurse
Remove-Item  -Path "$FDATA\$folderUI" -Force -recurse
Remove-Item  -Path "$Home\Desktop\Spectra-CH1.lnk"
Remove-Item  -Path "$Home\Desktop\Spectra-CH2.lnk"
