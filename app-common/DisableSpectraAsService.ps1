﻿# DisableSpectraAsService.ps1 script changes Spectra from Running as a Service to Running as User Space process
# To Playback Audio via Spectra Audio Monitoring
# It creates Shortcut SpectraAudio on Desktop with Run As Administrator set
# Shortcut SpectraAudio should be used to start Spectra Control panel if Audio Monitoring is Required
# Use EnableSpectraAsService.ps1 to re-enable Default Spectra as A Widnow Local Service mode

# Note1: It seems that Nice DCV installation don't require this and work Correctly in Local Service mode
# Note2: encoder.log will be created if C:\ProgramData\Streambox\SpectraUI\log\encoder.log exist when DisableSpectraAsService script runs
# to disable Logs for Usersapce mode, run DisableSpectraLogs.ps1 then re-run DisableSpectraAsService.ps1 or EnableSpectraAsService.ps1
# Note3:Use EnableSpectraAsService.ps1 to Revert to Widnow Local Service mode

# Check if running as Administrator
If (-NOT ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    # Relaunch as administrator
    Start-Process PowerShell -ArgumentList "-File `"$($MyInvocation.MyCommand.Definition)`"" -Verb RunAs
    exit
}

# Attempt to stop services silently
Stop-Service spectra3 -ErrorAction SilentlyContinue
Stop-Service spectra5 -ErrorAction SilentlyContinue

# Attempt to stop processes silently if they exist
$processes = @("Encoder5", "Encoder3","SpectraControlPanel")
foreach ($process in $processes) {
    $proc = Get-Process -Name $process -ErrorAction SilentlyContinue
    if ($proc) {
        $proc | Stop-Process -Force -ErrorAction SilentlyContinue
    }
}

# Backup the encassist.ini file
$sourceFile = "C:\Program Files\Streambox\Spectra\dist\encassist.ini"
$backupFile = $sourceFile + ".bak"
Copy-Item $sourceFile -Destination $backupFile -Force

# Read content excluding specific lines
$content = Get-Content $sourceFile | Where-Object { $_ -notmatch "cmd-ps-ws-stop-spectra|cmd-ps-ws-start-spectra|app-icon-is-enc-status|polling-apps-php-port" }

$newLines = @"
app-icon-is-enc-status = yes
polling-apps-php-port = yes
cmd-ps-ws-start-spectra5=C:\Program Files\Streambox\Spectra\Encoder5.exe -xml C:\ProgramData\Streambox\SpectraUI\settings.xml -logfile C:\ProgramData\Streambox\SpectraUI\log\encoder.log
cmd-ps-ws-stop-spectra5=C:\Windows\System32\windowspowershell\v1.0\powershell.exe Get-Process -Name "Encoder5" | Stop-Process
cmd-ps-ws-start-spectra3=C:\Program Files\Streambox\Spectra\Encoder3.exe -xml C:\ProgramData\Streambox\SpectraUI\settings.xml -logfile C:\ProgramData\Streambox\SpectraUI\log\encoder.log
cmd-ps-ws-stop-spectra3=C:\Windows\System32\windowspowershell\v1.0\powershell.exe Get-Process -Name "Encoder3" | Stop-Process
"@


$newLines_no_log = @"
app-icon-is-enc-status = yes
polling-apps-php-port = yes
cmd-ps-ws-start-spectra5=C:\Program Files\Streambox\Spectra\Encoder5.exe -xml C:\ProgramData\Streambox\SpectraUI\settings.xml 
cmd-ps-ws-stop-spectra5=C:\Windows\System32\windowspowershell\v1.0\powershell.exe Get-Process -Name "Encoder5" | Stop-Process
cmd-ps-ws-start-spectra3=C:\Program Files\Streambox\Spectra\Encoder3.exe -xml C:\ProgramData\Streambox\SpectraUI\settings.xml 
cmd-ps-ws-stop-spectra3=C:\Windows\System32\windowspowershell\v1.0\powershell.exe Get-Process -Name "Encoder3" | Stop-Process
"@

# Write back to the file
$content | Set-Content $sourceFile

# Check if the encoder.log file exists
if (Test-Path -Path "C:\ProgramData\Streambox\SpectraUI\log\encoder.log"
) {
    # encoder.log exists, add $newLines to the source file
    Add-Content -Path $sourceFile -Value $newLines
} else {
    # encoder.log does not exist, add $newLines_no_log to the source file
    Add-Content -Path $sourceFile -Value $newLines_no_log
}
#Add-Content -Path $sourceFile -Value $newLines




#Create Shortcut to SpectraAudio that runs in UserSpace as Administrator
$WshShell = New-Object -comObject WScript.Shell

$fshortcut = "$Home\Desktop\SpectraAudio.lnk"
$WshShell = New-Object -comObject WScript.Shell
#$FSTREAMBOX='$Env:Programfiles\Streambox'
$FSTREAMBOX='C:\Program Files\Streambox'
$Shortcut = $WshShell.CreateShortcut("$fshortcut")
$Shortcut.TargetPath = "$FSTREAMBOX\Spectra\dist\SpectraControlPanel.exe"
$Shortcut.WorkingDirectory =  "$FSTREAMBOX\Spectra\dist\"
$Shortcut.Save()

$bytes = [System.IO.File]::ReadAllBytes("$fshortcut")
$bytes[0x15] = $bytes[0x15] -bor 0x20 #set byte 21 (0x15) bit 6 (0x20) ON
[System.IO.File]::WriteAllBytes("$fshortcut", $bytes)
