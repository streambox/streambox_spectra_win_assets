// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "MediaCapture.h"

#include "ImageWriteTypes.h"
#include "SpectraCapture.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSpectraCapture, Log, All);

/**
 * 
 */
UCLASS()
class SPECTRACORE_API USpectraCapture : public UMediaCapture
{
	GENERATED_BODY()
public:
	double frameno;

	// initial w,h
	double 	sc_fps; 
	int    	sc_bits; // 8/16 bits
	int 	sc_pack; // color packing, 0 is not set
	int 	sc_w,sc_h; // resolution
	double  sc_start=0;//start time
	USpectraCapture() { 
		frameno=0; sc_w=1920;sc_h=1080;sc_fps=24;sc_bits=0;sc_pack=0;
		sc_start=GetSec();
	} 
	double GetSec();  // time-stamp

	/** Use the default back buffer size or specify a specific size to Encoder. */
	bool bOverrideEncoderSize;

	/** Use  original video resolution or  specific resolution for output to  Spectra Encoder. */
	FIntPoint DesiredEncoderSize;

	/** Specify frames per second for Spectra Encoder*/
	float EncoderFPS;

	/** DEBUG: Enable Onscreen Debug Messegase. */
	bool bEnableOnscreenMessages;
	
	/** DEBUG: Enable Conversion of 10-bit RGBA to 16-bit RGBA (VIDEO_L64A, with clipping support) */
	bool bEnable10bitTo16bitConversion;


	/** DEBUG: Spectra channel Number from 0 to 8, to output to multiple specra instances, 0 is default */
	int32 SpectraChannel;


protected:
	virtual void OnFrameCaptured_RenderingThread(const FCaptureBaseData& InBaseData, TSharedPtr<FMediaCaptureUserData, ESPMode::ThreadSafe> InUserData, void* InBuffer, int32 Width, int32 Height, int32 BytesPerRow) override;
	virtual bool InitializeCapture() override;
	/** Called at the beginning of the Capture_RenderThread call if output resource type is Texture */
	virtual void BeforeFrameCaptured_RenderingThread(const FCaptureBaseData& InBaseData, TSharedPtr<FMediaCaptureUserData, ESPMode::ThreadSafe> InUserData, FTextureRHIRef InTexture) override;


	/** Called after initialize for viewport capture type */
	virtual bool PostInitializeCaptureViewport(TSharedPtr<FSceneViewport>& InSceneViewport) override;

	/** Called after initialize for render target capture type */
	virtual bool PostInitializeCaptureRenderTarget(UTextureRenderTarget2D* InRenderTarget) override;

	virtual void OnRHIResourceCaptured_RenderingThread(const FCaptureBaseData& InBaseData, TSharedPtr<FMediaCaptureUserData, ESPMode::ThreadSafe> InUserData, FTextureRHIRef InTexture) override;
	virtual void OnRHIResourceCaptured_RenderingThread(const FCaptureBaseData& InBaseData, TSharedPtr<FMediaCaptureUserData, ESPMode::ThreadSafe> InUserData, FBufferRHIRef InBuffer) override;
  	virtual bool ShouldCaptureRHIResource() const override;
protected:
	virtual bool ValidateMediaOutput() const override;
	virtual bool UpdateSceneViewportImpl(TSharedPtr<FSceneViewport>& InSceneViewport) override;
	virtual bool UpdateRenderTargetImpl(UTextureRenderTarget2D* InRenderTarget) override;
	virtual void StopCaptureImpl(bool bAllowPendingFrameToBeProcess) override;



private:
	void CacheMediaOutputValues();
	void ApplyViewportTextureAlpha(TSharedPtr<FSceneViewport> InSceneViewport);
	void RestoreViewportTextureAlpha(TSharedPtr<FSceneViewport> InSceneViewport);

private:
	FString BaseFilePathName;
	EImageFormat ImageFormat;
	TFunction<void(bool)> OnCompleteWrapper;
	bool bOverwriteFile;
	int32 CompressionQuality;
	bool bAsync;
};
