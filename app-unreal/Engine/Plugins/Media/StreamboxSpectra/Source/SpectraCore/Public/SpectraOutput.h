// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "MediaOutput.h"

#include "Engine/RendererSettings.h"
#include "ImageWriteBlueprintLibrary.h"

#include "SpectraOutput.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSpectraOutput, Log, All);


/** Texture format supported by USpectraOutput. */
UENUM()
enum class ESpectraOutputPixelFormat
{
	B8G8R8A8					UMETA(DisplayName = "8bit  RGBA"),
	FloatRGBA					UMETA(DisplayName = "Float RGBA"),
//	FloatRGB					UMETA(DisplayName = "Float RGB"),
	A2R10G10B10					UMETA(DisplayName = "10bit ARGB"),
};


/**
 * Output information for a file media capture.
 * @note	'Frame Buffer Pixel Format' must be set to at least 8 bits of alpha to enabled the Key.
 * @note	'Enable alpha channel support in post-processing' must be set to 'Allow through tonemapper' to enabled the Key.
 */
UCLASS(BlueprintType)
class SPECTRACORE_API USpectraOutput : public UMediaOutput
{
	GENERATED_BODY()
	
public:
	USpectraOutput();

public:
	


	/** Use the default back buffer size or specify a specific size to capture. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Media", meta = (InlineEditConditionToggle))
	bool bOverrideDesiredSize;

	/** Use the default back buffer size or specify a specific size to capture. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Media", meta=(EditCondition="bOverrideDesiredSize"))
	FIntPoint DesiredSize;

	/** Use the default back buffer pixel format or specify a specific the pixel format to capture. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Media", meta = (InlineEditConditionToggle))
	bool bOverridePixelFormat;

	/** Use the default back buffer pixel format or specify a specific the pixel format to capture. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Media", meta=(EditCondition="bOverridePixelFormat"))
	ESpectraOutputPixelFormat DesiredPixelFormat;

	/** Invert the alpha for formats that support alpha. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Media")
	bool bInvertAlpha;

	/** Use the default back buffer size or specify a specific size to Encoder. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Output", meta = (InlineEditConditionToggle))
	bool bOverrideEncoderSize;

	/** Use  original video resolution or  specific resolution for output to  Spectra Encoder. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Output", meta=(EditCondition="bOverrideEncoderSize"))
	FIntPoint DesiredEncoderSize;

	
	/** Specify frames per second for Spectra Encoder*/
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Output")
	float EncoderFPS;


	/** DEBUG: Enable Onscreen Debug Messegase. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Development")
	bool bEnableOnscreenMessages;
	
	/** DEBUG: Enable Conversion of 10-bit RGBA ( will enforce) to 16-bit RGBA (VIDEO_L64A, with clipping support) */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Development")
	bool bEnable10bitTo16bitConversion;

	/** Spectra channel Number from 0 to 7, to output to multiple spectra instances, 0 is default. This version can support one channel per Unreal instance running. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Development")
	int32 SpectraChannel;

	//~ UMediaOutput interface
public:
	virtual bool Validate(FString& FailureReason) const override;
	virtual FIntPoint GetRequestedSize() const override;
	virtual EPixelFormat GetRequestedPixelFormat() const override;
	virtual EMediaCaptureConversionOperation GetConversionOperation(EMediaCaptureSourceType InSourceType) const override;

protected:
	virtual UMediaCapture* CreateMediaCaptureImpl() override;
	//~ End UMediaOutput interface
};
