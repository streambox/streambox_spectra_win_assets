// Copyright Streambox Inc. All Rights Reserved.
#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"


//class ISpectraCoreDeviceProvider;


/**
 * Definition the SpectraCore module.
 */
class SPECTRACORE_API ISpectraCoreModule : public IModuleInterface
{
public:
	static bool IsAvailable();
	static ISpectraCoreModule& Get();

public:
	//virtual void RegisterDeviceProvider(ISpectraCoreDeviceProvider* InProvider) = 0;
	//virtual void UnregisterDeviceProvider(ISpectraCoreDeviceProvider* InProvider) = 0;
	//virtual ISpectraCoreDeviceProvider* GetDeviceProvider(FName InProviderName) = 0;
	//virtual TConstArrayView<ISpectraCoreDeviceProvider*> GetDeviceProviders() const = 0;
};
