// Copyright Streambox Inc. All Rights Reserved.

namespace UnrealBuildTool.Rules
{
	public class SpectraEditor : ModuleRules
	{
		public SpectraEditor(ReadOnlyTargetRules Target) : base(Target)
		{
			PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"EditorFramework",
					"SpectraCore",
					"Slate",
					"SlateCore",
					"TimeManagement",
					"UnrealEd",
				});

			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Engine",
					"MediaAssets"
				});
		}
	}
}
