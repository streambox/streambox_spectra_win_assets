// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "Customizations/SpectraCustomizationBase.h"
#include "Input/Reply.h"
#include "SpectraCoreDefinitions.h"

/**
 * Implements a details view customization for the FSpectraInputConfiguration
 */
class SpectraEDITOR_API FSpectraInputConfigurationCustomization : public FSpectraCustomizationBase
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

private:
	virtual TAttribute<FText> GetContentText() override;
	virtual TSharedRef<SWidget> HandleSourceComboButtonMenuContent() override;

	void OnSelectionChanged(FSpectraInputConfiguration SelectedItem);
	FReply OnButtonClicked() const;

	TWeakPtr<SWidget> PermutationSelector;
	FSpectraInputConfiguration SelectedConfiguration;
};
