// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "Customizations/SpectraCustomizationBase.h"

/**
 * Implements a details view customization for the FSpectraDevice
 */
class SPECTRAEDITOR_API FSpectraDeviceCustomization : public FSpectraCustomizationBase
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

private:
	virtual TAttribute<FText> GetContentText() override;
	virtual TSharedRef<SWidget> HandleSourceComboButtonMenuContent() override;
};
