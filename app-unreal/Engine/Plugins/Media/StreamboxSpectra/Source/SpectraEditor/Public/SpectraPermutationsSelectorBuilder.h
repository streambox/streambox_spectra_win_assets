// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "SpectraCoreDefinitions.h"


struct SpectraEDITOR_API FSpectraPermutationsSelectorBuilder
{
	static const FName NAME_DeviceIdentifier;
	static const FName NAME_TransportType;
	static const FName NAME_QuadType;
	static const FName NAME_Resolution;
	static const FName NAME_Standard;
	static const FName NAME_FrameRate;

	static const FName NAME_InputType;
	static const FName NAME_OutputType;
	static const FName NAME_KeyPortSource;
	static const FName NAME_OutputReference;
	static const FName NAME_SyncPortSource;

	static bool IdenticalProperty(FName ColumnName, const FSpectraConnection& Left, const FSpectraConnection& Right);
	static bool Less(FName ColumnName, const FSpectraConnection& Left, const FSpectraConnection& Right);
	static FText GetLabel(FName ColumnName, const FSpectraConnection& Item);
	static FText GetTooltip(FName ColumnName, const FSpectraConnection& Item);
	
	static bool IdenticalProperty(FName ColumnName, const FSpectraMode& Left, const FSpectraMode& Right);
	static bool Less(FName ColumnName, const FSpectraMode& Left, const FSpectraMode& Right);
	static FText GetLabel(FName ColumnName, const FSpectraMode& Item);
	static FText GetTooltip(FName ColumnName, const FSpectraMode& Item);

	static bool IdenticalProperty(FName ColumnName, const FSpectraConfiguration& Left, const FSpectraConfiguration& Right);
	static bool Less(FName ColumnName, const FSpectraConfiguration& Left, const FSpectraConfiguration& Right);
	static FText GetLabel(FName ColumnName, const FSpectraConfiguration& Item);
	static FText GetTooltip(FName ColumnName, const FSpectraConfiguration& Item);

	static bool IdenticalProperty(FName ColumnName, const FSpectraInputConfiguration& Left, const FSpectraInputConfiguration& Right);
	static bool Less(FName ColumnName, const FSpectraInputConfiguration& Left, const FSpectraInputConfiguration& Right);
	static FText GetLabel(FName ColumnName, const FSpectraInputConfiguration& Item);
	static FText GetTooltip(FName ColumnName, const FSpectraInputConfiguration& Item);

	static bool IdenticalProperty(FName ColumnName, const FSpectraOutputConfiguration& Left, const FSpectraOutputConfiguration& Right);
	static bool Less(FName ColumnName, const FSpectraOutputConfiguration& Left, const FSpectraOutputConfiguration& Right);
	static FText GetLabel(FName ColumnName, const FSpectraOutputConfiguration& Item);
	static FText GetTooltip(FName ColumnName, const FSpectraOutputConfiguration& Item);
};
