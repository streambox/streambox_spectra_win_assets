// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "Customizations/SpectraCustomizationBase.h"
#include "SpectraCoreDefinitions.h"
#include "Input/Reply.h"

/**
 * Implements a details view customization for the video timecode configuration.
 */
class FSpectraVideoTimecodeConfigurationCustomization : public FSpectraCustomizationBase
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance() { return MakeShared<FSpectraVideoTimecodeConfigurationCustomization>(); }
	
private:
	virtual TAttribute<FText> GetContentText() override;
	virtual TSharedRef<SWidget> HandleSourceComboButtonMenuContent() override;

	void OnSelectionChanged(FSpectraVideoTimecodeConfiguration SelectedItem);
	FReply OnButtonClicked();
	ECheckBoxState GetAutoCheckboxState() const;
	void SetAutoCheckboxState(ECheckBoxState CheckboxState);
	bool ShowAdvancedColumns(FName ColumnName, const TArray<FSpectraVideoTimecodeConfiguration>& UniquePermutationsForThisColumn) const;
	bool IsAutoDetected() const;
	void SetIsAutoDetected(bool Value);

private:
	TWeakPtr<SWidget> PermutationSelector;
	FSpectraVideoTimecodeConfiguration SelectedConfiguration;
	bool bAutoDetectFormat = false;
};
