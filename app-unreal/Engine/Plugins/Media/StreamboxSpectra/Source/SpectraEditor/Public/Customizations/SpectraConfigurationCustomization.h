// Copyright Streambox Inc. All Rights Reserved.

#pragma once

#include "Customizations/SpectraCustomizationBase.h"
#include "Input/Reply.h"
#include "SpectraCoreDefinitions.h"

/**
 * Implements a details view customization for the FSpectraConfiguration
 */
class SPECTRAEDITOR_API FSpectraConfigurationCustomization : public FSpectraCustomizationBase
{
public:
	static TSharedRef<IPropertyTypeCustomization> MakeInstance();

private:
	virtual TAttribute<FText> GetContentText() override;
	virtual TSharedRef<SWidget> HandleSourceComboButtonMenuContent() override;

	ECheckBoxState GetAutoCheckboxState() const;
	void SetAutoCheckboxState(ECheckBoxState CheckboxState);
	void OnSelectionChanged(FSpectraConfiguration SelectedItem);
	FReply OnButtonClicked();
	bool ShowAdvancedColumns(FName ColumnName, const TArray<FSpectraConfiguration>& UniquePermutationsForThisColumn) const;
	bool IsAutoDetected() const;
	void SetIsAutoDetected(bool Value);

private:
	TWeakPtr<SWidget> PermutationSelector;
	FSpectraConfiguration SelectedConfiguration;
	bool bAutoDetectFormat = true;
};
