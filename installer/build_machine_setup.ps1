cd ~

choco feature enable -n allowGlobalConfirmation
choco install powershell-core 
choco install jq

curl.exe --silent http://169.254.169.254/latest/dynamic/instance-identity/document

choco install python golang

choco install make
$env:PATH += ";C:\ProgramData\chocolatey\lib\make\tools\install\bin"
make --version

choco install goreleaser
goreleaser --version
ls C:\ProgramData\chocolatey\bin\goreleaser.exe

choco install git
$env:PATH += ";C:\Program Files\Git\bin"
git --version

$global:ProgressPreference = "SilentlyContinue"
Invoke-WebRequest -OutFile ~/.gitconfig -Uri https://raw.githubusercontent.com/TaylorMonacelli/dotfiles/master/.gitconfig
@"
Set-Alias g git
"@ | Out-File -Append -Encoding ASCII $profile
. $profile

choco install golang 
$env:PATH+=';C:\Program Files\Go\bin'

choco install vscode
$env:PATH += ";C:\Program Files\Microsoft VS Code\bin"

go version
go install github.com/go-delve/delve/cmd/dlv@latest
go install github.com/ramya-rao-a/go-outline@latest
go install github.com/tpng/gopkgs@latest
go install golang.org/x/lint/golint@latest
go install golang.org/x/tools/gopls@latest
go install honnef.co/go/tools/cmd/staticcheck@latest
code --list-extensions
code --install-extension golang.go

code --install-extension ms-python.python

# setup profile

# powershell-core:
New-Item -Type "directory" -Force -Path ~/Documents/PowerShell | Out-Null

# powershell:
New-Item -Type "directory" -Force -Path ~/Documents/WindowsPowershell | Out-Null
@"
Set-Location ~
"@ | Out-File -Append -Encoding ASCII $profile
. $profile


# 
choco install python golang goreleaser git

# install wix
$global:ProgressPreference = "SilentlyContinue"
$prop = Get-Service wuauserv | Select -Property StartType
Set-Service -Name wuauserv -StartupType Manual
Install-WindowsFeature -Name NET-Framework-Features
Set-Service -Name wuauserv -StartupType $prop.StartType
choco install wixtoolset
$wix_dir = (Get-ChildItem -Recurse C:\Program*\Wix*Toolset*\bin -Filter "heat.exe" | Select-Object -First 1).Directory.FullName
Import-Module "$env:ChocolateyInstall\helpers\chocolateyInstaller.psm1" -Force
Install-ChocolateyPath -PathToInstall $wix_dir

git clone --quiet https://gitlab.com/streambox/removesalt.git

git submodule update --init --depth 10
git clone --quiet --depth 1 https://gitlab.com/streambox/streambox_iris_win_assets.git
git clone --quiet --depth 1 https://gitlab.com/streambox/streambox_mediaplayer_win_assets.git
git clone --quiet --depth 1 https://gitlab.com/streambox/spectra_encassist
git clone --quiet --depth 1 https://github.com/taylormonacelli/nightrover

cd removesalt
./setup.ps1
cd ..

cd nightrover
goreleaser build --single-target --snapshot --clean
cd ..
copy nightrover\dist\nightrover_windows_amd64_v1\nightrover.exe streambox_spectra_win_assets\app-common\nightrover.exe

removesalt --multirun pipeline=nosign +app=spectra pipeline.stop_all_uploads=$true
removesalt --multirun pipeline=nosign +app=spectra-cdi
removesalt --multirun pipeline=nosign +app=iris
removesalt --multirun pipeline=nosign +app=mediaplayer

# powershell core customization

# powershell-core:
New-Item -Type "directory" -Force -Path ~/Documents/PowerShell | Out-Null

# powershell:
New-Item -Type "directory" -Force -Path ~/Documents/WindowsPowershell | Out-Null
@"
Set-Location ~
"@ | Out-File -Append -Encoding ASCII $profile
. $profile
