# rg --files /Users/mtm/pdev/streambox/streambox_spectra_win_assets | grep allfiles >/tmp/released; rg --files /Users/mtm/pdev/streambox/streambox_spectra_win_assets | grep -v allfiles >/tmp/repo

import pathlib

released_path = pathlib.Path("/tmp/released")
repo_path = pathlib.Path("/tmp/repo")

released = {}
repo = {}

for line in released_path.read_text().splitlines():
    path = pathlib.Path(line)
    released[path.name.lower()] = pathlib.Path(line)

for line in repo_path.read_text().splitlines():
    path = pathlib.Path(line)
    repo[path.name.lower()] = path

for fname in released:
    source_path = released[fname.lower()]
    if not fname.lower() in repo:
        continue
    target_path = repo[fname.lower()]
    print(f"cp '{source_path}' '{target_path}'")
