Import-Module Pester -MinimumVersion 5.0.2

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
. "$here\setup.ps1"
. "$here\common.ps1"

&$verify_clean

Describe "DS026:verifies disabling blackmagic works as expected" -Tag "DS026" {
    BeforeAll {
        $name = "DS026"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        
        # Create Fake OpenIO_Blackmagic.acf and then ensure
        # OpenIO_Blackmagic.acf is removed upon install
        MkDir -Force "C:\Program Files\Avid\AVX2_Plug-ins" | Out-Null
        Set-Content -Value $null -Encoding ASCII -Path "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf"
    }
    AfterAll {
        If(Test-Path("C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled")){
            Remove-Item -Force "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled"
        }
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
        If(Test-Path("C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled")){
            Remove-Item -Force "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled"
        }
        $files = Get-ChildItem "C:\Program Files\Avid\AVX2_Plug-ins" 
        If(($files | Measure-Object).Count -eq 0){
            Remove-Item -Force "C:\Program Files\Avid\AVX2_Plug-ins" | Out-Null
        }
    }
    It "IT057:verifies C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf exists" {
        $path = "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf"
        $path | Should -Exist
    }
    It "IT058:verifies C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf does not exist after install" {
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
        $path = "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf"
        $path | Should -Not -Exist
    }
    It "IT059:verifies C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled exists after install" {
        $path = "C:\Program Files\Avid\AVX2_Plug-ins\OpenIO_Blackmagic.acf.disabled"
        $path | Should -Exist
    }
}

&$verify_clean

Describe "DS025:verifies setting.xml attributes" -Tag "DS025" -Skip {
    BeforeAll {
        $name = "DS025"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name

        $settings = "C:\ProgramData\Streambox\SpectraUI\settings.xml"
        $expected = "C:\ProgramData\Streambox\SpectraUI\hash"
        $testbasedir = pwd
        
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
        ./tests/settings-xml-check.ps1 -xmlpath $settings -testbasedir $testbasedir
        $tidiedpath = Join-Path $testbasedir settings_test | Join-Path -ChildPath settings-tidy-out.xml
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT055:verifies HashPath in settings.xml is present so activations are retained after uninstall" {
        [xml]$doc = Get-Content $tidiedpath
        $doc.root.SB_HD_ENCODER.HashPath | Should -Be $expected
    }
}

&$verify_clean

Describe "DS013:All files are put back after upgrade" -Tag "DS013" {
    BeforeAll {
        $name = "DS013"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_old_spectra_bundle $params
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    &$deploys_spectra_files2 # verifies expected files are deployed
    It "IT013-1:installs upgrade" {
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
    }
    &$deploys_spectra_files2 # verifies expected files are deployed
}

&$verify_clean

Describe "DS023:" -Tag "DS023" -Skip {
    BeforeAll {
        $name = "DS023"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_old_spectra_bundle $params
        $settings = 'C:\Program Files\Streambox\SpectraUI\settings.xml'
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT046:verifies settings/presets are not destroyed on update" {
       $hash1 = (Get-FileHash $settings).Hash
       Add-Content $settings " "
       $hash2 = (Get-FileHash $settings).Hash
       $hash1 | Should -Be $hash1
       $hash2 | Should -Be $hash2
       $hash1 | Should -Not -Be $hash2
       &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
       $hash3 = (Get-FileHash $settings).Hash
       $hash3 | Should -Be $hash2
    }
}

&$verify_clean

Describe "DS017:verifies upgrade will fix service from SERVICE_DELAYED_AUTO_START to SERVICE_DEMAND_START" -Tag "DS017" {
    BeforeAll {
        $name = "DS017"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name -old_version "0.9.28.0"
        &$install_old_spectra_bundle $params
        $services = @('Spectra3', 'Spectra5', 'spectra3', 'spectra5')
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT032-1:validates old version number" {
        $params.old_version | Should -Be "0.9.28.0"
    }
    It "IT032-2:validates old installer sets startup type incorrectly, namely to Automatic" {
        foreach ($service in $services) {
            (Get-Service $service).StartType | Should -Be 'Automatic'
        }
    }
    It "IT032-3:installs new spectra installer and verifies startup type is Manual (SERVICE_DEMAND_START)" {
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
        foreach ($service in $services) {
            (Get-Service $service).StartType | Should -Be 'Manual'
        }
    }
}

&$verify_clean

Describe "DS024:verifies spectra services are retained after upgrade" -Tag "DS024" {
    BeforeAll {
        $name = "DS024"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name -old_version "0.9.62.0"
        &$install_old_spectra_bundle $params
        $services = @('Spectra3', 'Spectra5', 'spectra3', 'spectra5')
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT047-1:validates old installer installs all services" {
        foreach ($service in $services) {
            (Get-Service | ?{ $_.Name -eq "$service" }).Count | Should -BeExactly 1
        }
    }
    It "IT047-2:installs new spectra installer and verifies all services are still present" {
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
        foreach ($service in $services) {
            (Get-Service | ?{ $_.Name -eq "$service" }).Count | Should -BeExactly 1
        }
    }
}

&$verify_clean

Describe "DS022:" -Tag "DS022" {
    BeforeAll {
        $name = "DS022"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT045:verifies control panel shortcut starts spectra panel" {
       start "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Spectra Control Panel.lnk"
       $is_running = Get-Process -Name SpectraControlPanel -ErrorAction SilentlyContinue
       $iter = 0
       While(!$is_running) {
           $iter += 1
           Start-Sleep -Seconds 5
           $is_running = Get-Process -Name SpectraControlPanel -ErrorAction SilentlyContinue
           if($iter -gt 3){
               break
           }
       }
       $results = Get-Process -Name SpectraControlPanel -ErrorAction SilentlyContinue
       $results.Count | Should -Be 1
    }
}

&$verify_clean

Describe "DS021:install old version, upgrade and verify version number is updated" -Tag "DS021","Slow" {
    BeforeAll {
        $name = "DS021"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        $version_file = Join-Path .. .bumpversion.cfg
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT044:verifies `$version_file is accessible" {
        $version_file | Should -Exist
    }
    It "IT039:verifies old version is recognized as expected" {
        &$install_old_spectra_bundle $params
        $prod = Get-CimInstance -Class Win32_Product | Where-Object { $_.Name -eq 'Spectra' }
        [version]$prod.Version | Should -Be $params.old_version
    }
    It "IT040:verifies I can parse version from .bumpversion.cfg" {
        $matcher = Select-String -Pattern "current_version\s*=\s*(\d+\.\d+\.\d+\.\d+)" -Path $version_file
        $latest = [version]$matcher.Matches.groups[1].Value
        $latest | Should -Not -BeNullOrEmpty
        $latest -gt $params.old_version | Should -Be $true
    }
    It "IT042:verifies upgrade bumps version number" {
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String

        $matcher = Select-String -Pattern "current_version\s*=\s*(\d+\.\d+\.\d+\.\d+)" -Path $version_file
        $latest = [version]$matcher.Matches.groups[1].Value
        $latest | Should -Not -BeNullOrEmpty

        $prod = Get-CimInstance -Class Win32_Product | Where-Object { $_.Name -eq 'Spectra' }
        [version]$prod.Version | Should -Be $latest
    }
    It "IT043:verifies vendor name is as expected" {
        $prod = Get-CimInstance -Class Win32_Product | Where-Object { $_.Name -eq 'Spectra' }
        $prod.Vendor | Should -Be 'Streambox'
    }
}

&$verify_clean

Describe "DS020:sanity check on pytest, make sure that if test fails, its reported in CI as failing" -Tag "DS020" {
    BeforeAll {
        $name = "DS020"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        . venv\Scripts\Activate.ps1
        $params = Get-Params -TestName $name
        $script = "${name}_error.py"
        $log = Join-Path $pwd "${script}.log"
        &$install_old_spectra_bundle $params
    }
    AfterAll {
        if(Test-Path($script)){ Remove-Item $script }
        &$params.old_installer /uninstall /passive /log $params.old_uninstaller_log | Out-String
    }
    It "IT038:verifies that we get error when python script has error" {
        Set-Content -Encoding ASCII -Path $script -Value "here is ERROR"
        &python $script 2>&1 | Out-File -Encoding ASCII $log
        Select-String -Path $log -Pattern "NameError" | Should -Not -Be $null
    }
}

&$verify_clean

Describe "DS019:set_defaults adds new defaults" -Tag "DS019" {
    BeforeAll {
        $name = "DS019"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        . venv\Scripts\Activate.ps1
        $params = Get-Params -TestName $name
        &$install_old_spectra_bundle $params
    }
    AfterAll {
        &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String
    }
    It "IT037-1:fresh install deploys default ini" {
        pytest --verbose test_set_defaults_consume.py
    }
    It "IT037-2:manually remove a paramter from ini and verifies new installer replaces it" {
        @'
"""
read ini to dict, remove dict item, save dict back to file
"""
import configparser, pathlib

ini_path = pathlib.Path(r"C:\Program Files\Streambox\Spectra\dist\encassist.ini")

config = configparser.RawConfigParser()
config.read(ini_path)

del config['encassist']['app_name']

with open(ini_path, 'w') as cf:
    config.write(cf)
'@ | Out-File -Encoding ASCII step1.py
        python step1.py

        @'
"""
read ini to dict, and verifies parameter we just removed is actually not there
"""
import configparser, pathlib

ini_path = pathlib.Path(r"C:\Program Files\Streambox\Spectra\dist\encassist.ini")

config = configparser.RawConfigParser()
config.read(ini_path)

assert 'app_name' not in config["encassist"]
'@ | Out-File -Encoding ASCII test_step1.py
        pytest --verbose test_step1.py
        &$params.new_installer /install /passive /log $params.new_installer_log | Out-String

        @'
"""
read ini to dict, and verifies parameter that we earlier removed, but
then re-added using installer is now present
"""

import configparser, pathlib

ini_path = pathlib.Path(r"C:\Program Files\Streambox\Spectra\dist\encassist.ini")

config = configparser.RawConfigParser()
config.read(ini_path)

assert 'app_name' in config['encassist']
'@ | Out-File -Encoding ASCII test_2_step1.py
        pytest --verbose test_2_step1.py
    }
}

&$verify_clean

Describe "DS018:verifies service startup is Manual" -Tag "DS018" {
    BeforeAll {
        $name = "DS018"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
    }
    AfterAll { &$uninstall_spectra_bundle $params }
    It "IT033:fresh installs spectra installer and verifies startup type is Manual (SERVICE_DEMAND_START)" {
        (Get-Service 'Spectra3').StartType | Should -Be 'Manual'
        (Get-Service 'Spectra5').StartType | Should -Be 'Manual'
    }
}

&$verify_clean

Describe "DS016:check set_defaults" -Tag "DS016" {
    BeforeAll {
        $name = "DS016"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
    }
    AfterAll { &$uninstall_spectra_bundle $params }

    It "IT032:verifies set_defaults.exe is called and runs as expected" {
        . venv\Scripts\Activate.ps1 #this gives us pytest
        pytest --verbose test_set_defaults_consume.py
    }
}

&$verify_clean

Describe "DS015:install spectra, start service Spectra5, verifies encoder5 is running" -Tag "DS015" {
    BeforeAll {
        $name = "DS015"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
        Start-Service -Name "Spectra5"
    }
    AfterAll { &$uninstall_spectra_bundle $params }

    &$spectra_encoder3_is_not_running
    &$spectra_encoder5_is_running
}

&$verify_clean

Describe "DS014:install spectra, start service Spectra3, verifies encoder3 is running" -Tag "DS014" {
    BeforeAll {
        $name = "DS014"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params

        Start-Service -Name "Spectra3"
    }
    AfterAll { &$uninstall_spectra_bundle $params }

    &$spectra_encoder5_is_not_running
    &$spectra_encoder3_is_running
}

&$verify_clean

Describe "DS004:verifies zip file contains what I expect" -Tag "zip", "DS004" {
    It "IT023:Zip contains both avid and universal bundles" {
        $sourceFile = Get-ChildItem -filter spectra.zip

        $sourceFile | Should -Not -BeNullOrEmpty
        Test-Path $sourceFile | Should -Be $true

        $entries = @()
        [Reflection.Assembly]::LoadWithPartialName("System.IO.Compression.FileSystem")
        [IO.Compression.ZipFile]::OpenRead($sourceFile.FullName).Entries.FullName | % { $entries += $_ }
        $entries.Count | Should -Be 5 # including directories
        $entries.Contains("Universal/spectra.exe") | Should -Be $true
        $entries.Contains("Avid/spectra.exe") | Should -Be $true
    }
}

&$verify_clean

Describe "DS005:install bundle, create log file, uninstall bundle, then log file should be removed" -Tag "log", "DS005" {
    BeforeAll {
        $name = "DS005"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
        Set-Content -Encoding ASCII -Value "my test" -Path "C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe.log"
        &$uninstall_spectra_bundle $params
    }
    AfterAll {
        Remove-Item -Force -ErrorAction Ignore "C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe.log"
    }
    &$removes_streambox_spectra_files
}

&$verify_clean

Describe "DS006:install bundle, don't start spectra service" -Tag "DS006" {
    BeforeAll {
        $name = "DS006"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
        $version = Get-InstalledSpectraVersion
    }
    AfterAll { &$uninstall_spectra_bundle $params }

    It "IT048:verifies C:\ProgramData\Streambox\SpectraUI\hash exists" -Tag "IT048" {
        if($version -ge [version]"0.9.76.7") {
            Test-Path "C:\ProgramData\Streambox\SpectraUI\hash" | Should -Be $true
        }
    }
    It "IT051:verifies C:\ProgramData\Streambox\SpectraUI\settings.xml exists" -Tag "IT051" {
        if($version -ge [version]"0.9.87.0") {
            Test-Path "C:\ProgramData\Streambox\SpectraUI\settings.xml" | Should -Be $true
        }
    }
    It "IT052:verifies C:\ProgramData\Streambox\SpectraUI\settings.xml.bak exists" -Tag "IT051" {
        if($version -ge [version]"0.9.87.0") {
            Test-Path "C:\ProgramData\Streambox\SpectraUI\settings.xml.bak" | Should -Be $true
        }
    }
    It "IT053:verifies C:\Program Files\Streambox\Spectra\settings.xml has been moved" -Tag "IT053" {
        if($version -ge [version]"0.9.87.0") {
            Test-Path "C:\Program Files\Streambox\Spectra\settings.xml" | Should -Be $false
        }
    }
    It "IT054:verifies C:\Program Files\Streambox\Spectra\settings.xml has been moved" -Tag "IT054" {
        if($version -ge [version]"0.9.87.0") {
            Test-Path "C:\Program Files\Streambox\Spectra\settings.xml.bak" | Should -Be $false
        }
    }
    It "IT049:verifies C:\Program Files\Streambox\Spectra\dist\chromactivate.exe exists" -Tag "IT049" {
        if($version -ge [version]"0.9.73.1") {
            Test-Path "C:\Program Files\Streambox\Spectra\dist\chromactivate.exe" | Should -Be $true
        }
    }
    It "IT060:verifies C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources\com.Streambox.Spectra.png exists" -Tag "IT060" {
        if($version -ge [version]"0.9.153.0") {
            Test-Path "C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources\com.Streambox.Spectra.png" | Should -Be $true
        }
    }
    It "IT061:verifies C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx exists" -Tag "IT061" {
        if($version -ge [version]"0.9.153.0") {
            Test-Path "C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx" | Should -Be $true
        }
    }
    &$deploys_spectra_files
    &$deploys_adobe_premiere_extension
    &$avid_extension_is_deployed
    &$spectra_service_is_not_running
    &$spectra_encoder_is_not_running
}

&$verify_clean

Describe "DS007:install bundle without adobe, verifies universal is not present" -Tag "DS007" {
    BeforeAll {
        $name = "DS007"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_only_avid $params
    }
    AfterAll { &$uninstall_spectra_bundle_with_only_avid $params }

    &$does_not_deploy_adobe_premiere_extension
    &$avid_extension_is_deployed
    &$deploys_spectra_files
}

&$verify_clean

Describe "DS009:install MSI, don't start spectra service" -Tag "DS009" {
    BeforeAll {
        $name = "DS009"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_msi_but_dont_start_service $params
    }
    AfterAll { &$uninstalls_spectra_msi $params }

    &$deploys_spectra_files
    &$deploys_adobe_premiere_extension
    &$avid_extension_is_deployed
    &$spectra_service_is_not_running
    &$spectra_encoder_is_not_running
}

&$verify_clean

Describe "DS_KEEP_AS_LAST_TEST:grep for errors in all log files" -Tag "ErrorsInLogs", "DS_KEEP_AS_LAST_TEST" {
    BeforeAll {
        $name = "DS_KEEP_AS_LAST_TEST"
        . $PSCommandPath.Replace("Workflow1.Tests", "setup")
        $params = Get-Params -TestName $name
        &$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service $params
        &$uninstall_spectra_bundle $params
        # exclude logs with string 'ProductVersion = 0.9.28.0' because we know those already have the error
        $exclude = Get-ChildItem *.log | Select-String "ProductVersion = $($params.old_version.ToString())" -List | Select Path | Split-Path -Leaf
        $candidates = Get-ChildItem *.log | Split-Path -Leaf | Where-Object { $_ -NotIn $exclude }
    }
    It "IT026:free from error '<message>'" -TestCases @(
        @{ message = "Cannot find path 'C:\Program Files\Streambox\Spectra\'" },
        @{ message = "Error 0x80070001" },
        @{ message = "CommandNotFoundException" },
        @{ message = "CustomAction UninstallService returned actual error code 1603 but will be translated to success due to continue marking" }
    ) {
        $results = Get-Content $candidates | Where-Object { $_.Contains($message) }
        $results.Count | Should -Be 0
    }
}

&$verify_clean
