import configparser
import pathlib

ini_path = pathlib.Path("Work1/dist/encassist.ini")


def test_ini_powershell_size_not_zero():
    assert ini_path.stat().st_size > 200


def test_ini_is_parseable():
    ini = configparser.RawConfigParser()
    ini.read(ini_path)
    dct = dict(ini.items("encassist"))

    assert ini["encassist"]["app_name"] == "Spectra Control Panel"
    assert len(dct.keys()) > 3
