function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = '0.25.0.0'

cd C:\Windows\temp
$url = "https://streambox-spectra.s3-us-west-2.amazonaws.com/${version}/win/spectra_win_${version}.zip"
$filename = Split-Path -Leaf -Path $url
$fileinfo = New-Object System.IO.FileInfo($filename)
Get-FileIfNotExists $url $filename
If($fileinfo.Extension.ToLower() -eq '.zip' -and (Test-Path($filename))){ Expand-Archive -Force $filename -DestinationPath $fileinfo.Basename}

&"$($fileinfo.Basename)/Universal/spectra.exe" /install /passive /log "spectra_install_${version}.log" | Out-String
