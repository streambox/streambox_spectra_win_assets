function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = '1.69.5.0'

cd C:\Windows\temp
$url = "https://streambox-spectra.s3-us-west-2.amazonaws.com/${version}/win/cdi/streambox_spectra_cdi_win.zip"
$filename = Split-Path -Leaf -Path $url
New-Item -Type "directory" -Force -Path spectra_win_${version} | Out-Null
Get-FileIfNotExists $url "spectra_win_${version}/$filename"
