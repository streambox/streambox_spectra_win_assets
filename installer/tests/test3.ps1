cd C:\Windows\temp
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -UseBasicParsing -Uri https://streambox-spectra.s3-us-west-2.amazonaws.com/0.16.0.0/win/spectra_win_0.16.0.0.zip -Outfile spectra_win_0.16.0.0.zip
Expand-Archive -Force spectra_win_0.16.0.0.zip
./Universal/spectra.exe /install /passive /log spectra_install_v0.16.0.0.log | Out-String

Get-Content C:\ProgramData\Streambox\SpectraUI\settings.xml

@'
test
'@ | Out-File -encoding ASCII C:\ProgramData\Streambox\SpectraUI\settings.xml

$glob = 'C:\projects\removesalt*\removesalt'
$dir = Get-ChildItem $glob | Select-Object -Expand Fullname
. $dir\build\.venv\Scripts\activate.ps1
$glob = "C:\projects\removesalt*\removesalt\build\spectra\s3\streambox-spectra\win\*\streambox_spectra_win.zip"
$zip = Get-ChildItem -Recurse $glob
Expand-Archive -Force $zip
./spectra_universal_win.exe /install /passive /log spectra_install_2.log | Out-String

Get-Content C:\ProgramData\Streambox\SpectraUI\settings.xml
