$paths = "C:\Program Files\Streambox\Spectra","C:\Programdata\Streambox\Spectra"

$manifest = Get-ChildItem -Recurse $paths | Select-Object -Expand Fullname
$manifest

# these can't be signed
$check_signed = $manifest | Where-Object {$_ -notmatch '.*\.(txt|bmp|lnk|ini)$'}

signtool verify /v /pa $check_signed
