cd C:\Windows\temp
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$ProgressPreference = 'SilentlyContinue'
Invoke-WebRequest -UseBasicParsing -Uri https://streambox-spectra.s3-us-west-2.amazonaws.com/0.16.0.0/win/spectra_win_0.16.0.0.zip -Outfile spectra_win_0.16.0.0.zip
Expand-Archive -Force spectra_win_0.16.0.0.zip
./Universal/spectra.exe /install /passive /log spectra_install_1.log | Out-String

Get-Content C:\ProgramData\Streambox\SpectraUI\version.txt

@'
test
'@ | Out-File -encoding ASCII C:\ProgramData\Streambox\SpectraUI\version.txt

$glob = 'C:\projects\removesalt*\removesalt'
$dir = Get-ChildItem $glob | Select-Object -Expand Fullname
. $dir\build\.venv\Scripts\activate.ps1
$glob = "C:\projects\removesalt*\removesalt\build\spectra\s3\streambox-spectra\win\*\streambox_spectra_win.zip"
$zip = Get-ChildItem -Recurse $glob
Expand-Archive -Force $zip
./spectra_universal_win.exe /install /passive /log spectra_install_2.log | Out-String

Get-Content C:\ProgramData\Streambox\SpectraUI\version.txt

@'
' Retrieve all ProductCodes (with ProductName and ProductVersion)
' Save on desktop as "MsiRetrieveProductCode.vbs" (extension important) and run from there by double-clicking it
' Output: "msiinfo.csv" on desktop itself. Open with Notepad, Excel or equivalent (Notepad will do)

Set fso = CreateObject("Scripting.FileSystemObject")
Set output = fso.CreateTextFile("msiinfo.csv", True, True)
Set installer = CreateObject("WindowsInstaller.Installer")

On Error Resume Next ' we ignore all errors

' Write headers
output.writeline ("ProductCode" & ", " & "ProductName" & ", " & "Version")

For Each product In installer.ProductsEx("", "", 7)
   productcode = product.ProductCode
   name = product.InstallProperty("ProductName")
   version=product.InstallProperty("VersionString")
   output.writeline (productcode & ", " & name & ", " & version)
Next

output.Close

'@ | Out-File -encoding ASCII versions.vbs
cscript versions.vbs
ls msiinfo.csv
$apps = Import-CSV -Path msiinfo.csv
$app = $apps | Where-Object { $_.ProductName -eq "Spectra" }
$app.ProductCode
# uninstall spectra
if($app) { Start-Process -Wait -FilePath "C:\Windows\System32\msiexec.exe" -ArgumentList "/x $($app.ProductCode) /passive /quiet /noreboot /L*V spectra_uninstall.log" }
