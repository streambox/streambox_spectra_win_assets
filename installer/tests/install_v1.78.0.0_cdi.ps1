function Get-FileIfNotExists {
    Param (
        $Url,
        $Destination
    )

    if (-not (Test-Path $Destination)) {
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $ProgressPreference = 'SilentlyContinue'

        Write-Verbose "Downloading $Url"
        Invoke-WebRequest -UseBasicParsing -Uri $url -Outfile $Destination
    }
    else {
        Write-Verbose "${Destination} already exists. Skipping."
    }
}

$version = '1.78.0.0'

cd C:\Windows\temp
$url = "https://streambox-spectra.s3-us-west-2.amazonaws.com/${version}/win/cdi/streambox_spectra_cdi_win_${version}.zip"
$filename = Split-Path -Leaf -Path $url
Get-FileIfNotExists $url $filename
Expand-Archive -Force $filename

&"streambox_spectra_cdi_win_${version}/streambox_spectra_cdi_win_${version}/spectra_cdi.exe" install /passive /log "streambox_spectra_cdi_win_${version}.log" | Out-String
