$spectra_service3_is_running = {
    $services = Get-Service | Where-Object { $_.Name -eq "Spectra3" -and $_.Status -eq "Running" }
    ($services | Measure-Object).Count | Should -Be 1
}

$spectra_service5_is_running = {
    $services = Get-Service | Where-Object { $_.Name -eq "Spectra5" -and $_.Status -eq "Running" }
    ($services | Measure-Object).Count | Should -Be 1
}

$spectra_service_is_not_running = {
    It "IT004:verifies spectra service is not running" -Tag "IT004" {
        $services = Get-Service | Where-Object { $_.Name -eq "Spectra3" -and $_.Status -eq "Running" }
        ($services | Measure-Object).Count | Should -Be 0
        $services = Get-Service | Where-Object { $_.Name -eq "Spectra5" -and $_.Status -eq "Running" }
        ($services | Measure-Object).Count | Should -Be 0
    }
}

$spectra_service_does_not_exist = {
    It "IT034:verifies spectra service does not exist" -Tag "IT034" {
        $services = Get-Service | Where-Object { $_.Name -eq "Spectra3" }
        ($services | Measure-Object).Count | Should -Be 0
        $services = Get-Service | Where-Object { $_.Name -eq "Spectra5" }
        ($services | Measure-Object).Count | Should -Be 0
    }
}

$verify_clean = {
    Describe "DS001:verify clean state" -Tag "verify_clean", "DS001" {
        &$removes_streambox_spectra_files
        &$removes_registry_keys
        &$removes_shortcuts
        &$obsolete_paths_are_gone
        &$avid_extension_is_not_deployed
        &$does_not_deploy_adobe_premiere_extension
        &$spectra_service_is_not_running
        &$spectra_encoder_is_not_running
        &$spectra_service_does_not_exist
    }   
}

$deploys_adobe_premiere_extension = {
    It "IT002:verifies Adobe extension is present" -Tag "IT002" { 
        Test-Path "C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore" | Should -Be $true
        Test-Path "C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore\Spectra.prm" | Should -Be $true
    }
}

$does_not_deploy_adobe_premiere_extension = {
    It "IT003:verifies Adobe extension is not present" -Tag "IT003" { 
        Test-Path "C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore" | Should -Be $false
        Test-Path "C:\Program Files\Adobe\Common\Plug-ins\7.0\MediaCore\Spectra.prm" | Should -Be $false
    }
}

$spectra_encoder_is_not_running = {
    &$spectra_encoder3_is_not_running
    &$spectra_encoder5_is_not_running
}

$spectra_encoder3_is_running = {
    It "IT006:verifies spectra encoder3 is running" -Tag "IT006" { 
        $processes = Get-Process | Where-Object { $_.Name -eq "Encoder3" } 
        ($processes | Measure-Object).Count | Should -Be 1
    }
}

$spectra_encoder5_is_running = {
    It "IT027:verifies spectra encoder5 is running" -Tag "IT027" { 
        $processes = Get-Process | Where-Object { $_.Name -eq "Encoder5" } 
        ($processes | Measure-Object).Count | Should -Be 1
    }
}

$spectra_encoder5_is_not_running = {
    It "IT030:verifies spectra encoder5 is not running" -Tag "IT030" { 
        $processes = Get-Process | Where-Object { $_.Name -eq "Encoder5" } 
        ($processes | Measure-Object).Count | Should -Be 0
    }
}

$spectra_encoder3_is_not_running = {
    It "IT032:verifies spectra encoder3 is not running" -Tag "IT033" { 
        $processes = Get-Process | Where-Object { $_.Name -eq "Encoder3" } 
        ($processes | Measure-Object).Count | Should -Be 0
    }
}

$avid_extension_is_deployed = {
    It "IT007:verifies avid extension is present" -Tag "IT007" { 
        Test-Path "C:\Program Files\Avid\AVX2_Plug-ins" | Should -Be $true
        Test-Path "C:\Program Files\Avid\AVX2_Plug-ins\Spectra.acf" | Should -Be $true
    }
}

$avid_extension_is_not_deployed = {
    It "IT008:verifies avid extension is not present" -Tag "IT008" { 
        Test-Path "C:\Program Files\Avid\AVX2_Plug-ins" | Should -Be $false
        Test-Path "C:\Program Files\Avid\AVX2_Plug-ins\Spectra.acf" | Should -Be $false
    }
}

$spectra_service_is_running = {
    It "IT029:verifies avid extension is not present" -Tag "IT029" { 
        &$spectra_service3_is_running
        &$spectra_service5_is_running
    }
}

$paths = @{
    deployed = @(
        @{ path = "C:\ProgramData\Streambox"; added_in_version = [version]"0.9.76.7" },
        @{ path = "C:\ProgramData\Streambox\SpectraUI"; added_in_version = [version]"0.9.76.7" },
        @{ path = "C:\ProgramData\Streambox\SpectraUI\hash"; added_in_version = [version]"0.9.76.7" },
        @{ path = "C:\ProgramData\Streambox\SpectraUI\liveServers.xml"; added_in_version = [version]"0.9.88.0" },
        @{ path = "C:\ProgramData\Streambox\SpectraUI\settings.xml"; added_in_version = [version]"0.9.88.0" },
        @{ path = "C:\ProgramData\Streambox\SpectraUI\settings.xml.bak"; added_in_version = [version]"0.9.88.0" },
        @{ path = "C:\Program Files\Streambox" },
        @{ path = "C:\Program Files\Streambox\Spectra" },
        @{ path = "C:\Program Files\Streambox\Spectra\dist" },
        @{ path = "C:\Program Files\Streambox\Spectra\dist\encassist.ini" },
        @{ path = "C:\Program Files\Streambox\Spectra\dist\chromactivate.exe"; added_in_version = [version]"0.9.73.1" },
        @{ path = "C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe" },
        @{ path = "C:\Program Files\Streambox\Spectra\service.log" },
        @{ path = "C:\Program Files\Streambox\Spectra\service-error.log" },
        @{ path = "C:\Program Files\Streambox\Spectra\Encoder3.exe" },
        @{ path = "C:\Program Files\Streambox\Spectra\Encoder5.exe" },
        @{ path = "C:\Program Files\Streambox\Spectra\nssm.exe" },
        @{ path = "C:\Program Files\Streambox\Spectra\sbxcmd.exe" }
    )
    globs = @(
        @{ path = "C:\Program Files\Streambox\Spectra\dist\SpectraControl*.log*" }
    )
    shortcuts = @(
        @{ path = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Spectra Control Panel.lnk" }
    )
}

$deploys_spectra_files2 = {
    It "IT010:verifies spectra files are deployed" -Tag "IT010" -TestCases $paths.deployed { 
        if($added_in_version -ne $null -and (Get-InstalledSpectraVersion) -ge $added_in_version) {
            $path | Should -Exist
        }
    }
    It "IT035:verifies shortcuts are created" -Tag "IT010" -TestCases $paths.shortcuts { 
        $path | Should -Exist
    }
    It "IT036:verifies spectra glob <path> exist" -Skip -Tag "IT010" -TestCases $paths.globs { 
        Test-Path($path) | Should -Be $true
    }
    &$obsolete_paths_are_gone
    &$deploys_registry_keys
}

$deploys_spectra_files = {
    It "IT010:verifies spectra files are deployed" -Tag "IT010" { 
        Test-Path "C:\Program Files\Streambox" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\dist" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\dist\encassist.ini" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\service.log" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\service-error.log" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder3.exe" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder5.exe" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\nssm.exe" | Should -Be $true
        Test-Path "C:\Program Files\Streambox\Spectra\sbxcmd.exe" | Should -Be $true
        Test-Path "C:\ProgramData\Streambox\SpectraUI\settings.xml" | Should -Be $true
        Test-Path "C:\ProgramData\Streambox\SpectraUI\settings.xml.bak" | Should -Be $true
    }
    It "IT011:verifies shortcuts are created" -Tag "IT011" { 
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Spectra Control Panel.lnk" | Should -Be $true
    }
    &$obsolete_paths_are_gone
    &$deploys_registry_keys
}

$removes_streambox_spectra_files = {
    It "IT012:verifies spectra files are removed upon uninstall" -Tag "IT012" {
#        Test-Path "C:\ProgramData\Streambox" | Should -Be $false # activation data is stored here for case where you uninstall, and re-install but don't want to activate again
#        Test-Path "C:\ProgramData\Streambox\Spectra" | Should -Be $false # activation data is stored here for case where you uninstall, and re-install but don't want to activate again
#        Test-Path "C:\ProgramData\Streambox\Spectra\hash" | Should -Be $false # activation data is stored here for case where you uninstall, and re-install but don't want to activate again
        Test-Path "C:\Program Files\Streambox" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\dist" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\dist\encassist.ini" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\dist\chromactivate.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\dist\SpectraControlPanel.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\dist\SpectraControl*.log*" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\service.log" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\service-error.log" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\UHD_Null.txt" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\UHD.txt" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder3.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder5.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\nssm.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\sbxcmd.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\settings.xml" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\settings.xml.bak" | Should -Be $false
    }
}

$deploys_registry_keys = {
    It "IT013:verifies registry keys are present" -Tag "IT013" {
        # FIXME what the heck are these for?
        #        Test-Path HKMU:\Software\Streambox | Should -Be $true
        #        Test-Path HKMU:\Software\Streambox\Spectra | Should -Be $true
        #        Test-Path HKMU:\Software\Streambox\Spectra\StartPanel | Should -Be $true
        #        (Get-ItemProperty -Path HKMU:\Software\Streambox\Spectra\StartACTL5 -Name installed).installed | Should -Be 1
    }
}

$removes_registry_keys = {
    It "IT001:verifies registry keys are not present" -Tag "IT001" { 
        Test-Path HKMU:\Software\Streambox | Should -Be $false
        Test-Path HKMU:\Software\Streambox\Spectra | Should -Be $false
        Test-Path HKMU:\Software\Streambox\Spectra\StopSpectra | Should -Be $false
        Test-Path HKMU:\Software\Streambox\Spectra\StartSpectra | Should -Be $false
    }
}

$shortcuts_have_run_as_administrator_property_set = {
    It "IT014:verifies shortcuts have property 'run as administrator'" -Tag "IT014" {
        $shortcut = "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Spectra Control Panel.lnk"
        $bytes = [System.IO.File]::ReadAllBytes($shortcut)
        $bytes[0x15] | Should -Be 96
    }
}

$obsolete_paths_are_gone = {
    It "IT016:verifies obsolete paths are still not present as expected" -Tag "IT016" { 
        # ensure old shortcuts and other files are gone
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder3org.exe" | Should -Be $false
        Test-Path "C:\Program Files\Streambox\Spectra\Encoder5org.exe" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Start Spectra.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Stop Spectra.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\START Spectra Service.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\STOP Spectra Service.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\STOP Spectra.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Select ACT-L3.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Select ACT-L5.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\SpectraPanel.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Start Spectra ACT-L3.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Start Spectra ACT-L5.lnk" | Should -Be $false
        Test-Path HKCU:\Software\Streambox | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\ControlPanel | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\StartACTL3 | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\StartACTL5 | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\StartService | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\StopService | Should -Be $false
        Test-Path HKCU:\Software\Streambox\Spectra\StopSpectra | Should -Be $false
    }
}

$removes_shortcuts = {
    It "IT015:verifies shortcuts don't exist" -Tag "IT015" { 
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Start Spectra.lnk" | Should -Be $false
        Test-Path "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Streambox\Spectra\Stop Spectra.lnk" | Should -Be $false
    }
}
