$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$common = Join-Path $here "\..\common.psm1"
Import-Module $common
Import-Module 7Zip4Powershell

Function Get-InstalledSpectraVersion {
    $Apps = @()
    $Apps += Get-ItemProperty "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*" # 32 Bit
    $Apps += Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*" # 64 Bit

    $version = [version]($Apps | Where-Object { $_.DisplayName -eq 'Spectra' }).DisplayVersion
    $version
}

Function Get-Params {
    param(
        [Parameter(Mandatory=$true)][string]$TestName,
        [Parameter(Mandatory=$false)][version]$old_version = "0.9.28.0"
    )

    @{
        TestName                      = $TestName

        old_version                   = $old_version

        old_installer                 = "$(pwd)\$($old_version)\Avid\spectra.exe"
        old_installer_log             = "$($TestName)_install_$($old_version).log"
        old_uninstaller_log           = "$($TestName)_uninstall_$($old_version).log"

        new_installer                 = "$(pwd)\Output0\Avid\spectra.exe"
        new_installer_log             = "$($TestName)_install_new.log"
        new_uninstaller_log           = "$($TestName)_uninstall_new.log"

        new_universal_installer           = "$(pwd)\Output0\Universal\spectra.exe"
        new_universal_installer_log       = "$($TestName)_install_new.log"
        new_universal_uninstaller_log     = "$($TestName)_uninstall_new.log"

        new_universal_msi                 = "$(pwd)\Output1\Universal\spectra.msi"
        new_universal_msi_installer_log   = "$($TestName)_install_new.log"
        new_universal_msi_uninstaller_log = "$($TestName)_uninstall_new.log"

        new_avid_msi                  = "$(pwd)\Output1\Avid\spectra.msi"
        new_avid_msi_installer_log    = "$($TestName)_install_new.log"
        new_avid_msi_uninstaller_log  = "$($TestName)_uninstall_new.log"
    }
}

$install_msi_but_dont_start_service = {
    param($params)
    &msiexec /i $params.new_universal_msi /passive /l*v $params.new_universal_msi_installer_log | Out-String  # force pester to wait till install completes
}

$uninstalls_spectra_msi = {
    param($params)
    $msi = "$(pwd)\Output1\Universal\spectra.msi"

    &msiexec /x $params.new_universal_msi /passive /l*v $params.new_universal_msi_uninstaller_log | Out-String  # force pester to wait till install completes
}

$installs_spectra_msi_with_only_avid_dont_start_service = {
    param($params)

    $msi = "$(pwd)\Output1\Avid\spectra.msi"
    &msiexec /i $msi /passive /l*v "$($params.TestName)_install.log" | Out-String  # force pester to wait till install completes
}

$uninstalls_spectra_msi_with_only_avid = {
    param($params)

    $msi = "$(pwd)\Output1\Avid\spectra.msi"
    &msiexec /x $msi /passive /l*v "$($params.TestName)_uninstall.log" | Out-String  # force pester to wait till install completes
}

$uninstall_spectra_bundle = {
    param($params)

    &$params.new_universal_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String  # force pester to wait till install completes
}

$install_spectra_bundle_with_only_avid = {
    param($params)
    
    &$params.new_installer /install /passive /log $params.new_installer_log  | Out-String  # force pester to wait till install completes
}

$uninstall_spectra_bundle_with_only_avid = {
    param($params)

    &$params.new_installer /uninstall /passive /log $params.new_uninstaller_log | Out-String  # force pester to wait till install completes
}

$install_spectra_bundle_with_both_extensions_and_dont_start_spectra_service = {
    param($params)

    &$params.new_universal_installer /install /passive /log $params.new_installer_log | Out-String  # force pester to wait till install completes
}

$install_old_spectra_bundle = {
    param($params)

    $zip = "spectra.zip"
    $url = "https://streambox-spectra.s3-us-west-2.amazonaws.com/$($params.old_version)/$zip"

    MkDir -Force $params.old_version | Out-Null
    $basedir = Resolve-Path $params.old_version
    $zip_path = Join-Path $basedir $zip

    if (!(Test-Path($zip_path))) {
        $wc = New-Object System.Net.WebClient
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        $wc.DownloadFile($url, $zip_path)
    }
    if (!(Test-Path($params.old_installer))) {
        Expand-7Zip -ArchiveFileName $zip_path -TargetPath $params.old_version
    }

    &$params.old_installer /install /passive /log "$($params.TestName)_install_$($params.old_version).log" | Out-String  # force pester to wait till install completes
}
