As of Installer v1.23 and above 
```powershell
https://streambox-spectra.s3-us-west-2.amazonaws.com/1.23.0.0/win/streambox_spectra_win_1.23.0.0.zip
```

the installer deploys these files:
```powershell
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Resources\com.Streambox.Spectra.png
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.NoCUDA\Contents\Resources\com.Streambox.Spectra.png
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.NoCUDA\Contents\Win64\SpectraPlugin.ofx
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.Universal\Contents\Resources\com.Streambox.Spectra.png
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.Universal\Contents\Win64\SpectraPlugin.ofx
```

of those files, these files are the same:
```powershell
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx
C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.Universal\Contents\Win64\SpectraPlugin.ofx
```

but if you want to try OFX v.24.18.NoCUDA, you could swap to v.24.18.NoCUDA like this:
```powershell
Copy-Item -Force `
"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.NoCUDA\Contents\Win64\SpectraPlugin.ofx" `
"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx"
```

To return back to original state v.24.18.Universal, you could do this:
```powershell
Copy-Item -Force `
"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\v.24.18.Universal\Contents\Win64\SpectraPlugin.ofx" `
"C:\Program Files\Common Files\OFX\Plugins\SpectraPlugin.ofx.bundle\Contents\Win64\SpectraPlugin.ofx"
```

or you could re-run v1.23 installer and click repair button.

If you install v1.24 or above, then your files will be reverted to the original state--namely using v.24.18.Universal, so you'd have to repeat this process to try v.24.18.NoCUDA again.
